## Kumpulan Template/Artboard untuk Inkscape.

* blog-banner.svg
* blog-grafis.svg
* facebook-iklan.svg
* facebook-sampul.svg
* ikon-toko-etsy.svg
* instagram-cerita.svg
* instagram-kiriman-persegi.svg
* instagram-kiriman-potrait.svg
* instagram-kiriman-slide-dobel.svg
* kartu-nama.svg
* kartu-pos.svg
* kartu.svg
* kolase-foto.svg
* kupon-hadiah.svg
* label.svg
* linkedin-banner.svg
* logo-artboard
* logo.svg
* menu.svg
* mikrostock.svg
* penanda-buku.svg
* pengumuman.svg
* pinterest-grafis.svg
* poster.svg
* presentasi-16:9.svg
* presentasi-4:3.svg
* resume.svg
* selebaran.svg
* sertifikat.svg
* snapchat-geofilter.svg
* tumblr-grafis.svg
* twitter-kiriman.svg
* twitter-tajuk.svg
* undangan.svg
* undangan-tegak.svg
* wallpaper-desktop.svg
* web-portofolio.svg
* whatsapp-status.svg
* youtube-gambar-mini.svg

## Catatan

* Ekspor PNG dengan **96** DPI untuk hasil standar (cukup).
* Untuk hasil yang maksimal gunakanlah **300** DPI.

**Silahkan Fork, Semoga Bermanfaat. ^^**
